//Посчитать количество нолей в массиве.
let m = [1,0,3,0,4,5,0,6,7];
function counter(mas,i){

    if(i === undefined){
        i = 0;
    }
    if(mas.length <= i){
        return 0;
    }
    let count = 0;
    if (mas[i] ===0){
        count++;
    }
    return counter(mas,++i)+count;
}
let count = counter(m);
console.log(count);