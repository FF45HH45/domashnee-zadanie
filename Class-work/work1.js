//Функции на добавление,удаление,вывод и вычесление среднего балла  студентов с массива.
var students = (function (){
    var mas = [];

    function add(student){
        mas.push(student);
    }

    function print(){
        console.log(mas);
    }

    function remove(firstname){
        for(var i = 0; i < mas.length; i++) {
            if (mas[i].firstname === firstname) {
                mas.splice(i, 1);
                break;
            }
        }
    }

    function avgEstimate() {
        var sum = 0;
        for (var i = 0; i < mas.length; i++) {
            sum += mas[i].estimate;
        }

        return sum / mas.length;
    }

    function getStudentsByEstimate(){
        var res = [];
        var evg = avgEstimate();
        for (var i = 0; i < mas.length; i++) {
            if(mas[i].estimate > evg){
                res.push(mas[i]);
            }
        }
        return res;
    }

    return {
        add: add,
        print: print,
        remove: remove,
        avgEstimate: avgEstimate,
        getStudentsByEstimate: getStudentsByEstimate
    };
})();
var st = {
    firstname: "Anton",
    estimate: 4.5,
};

var st2 = {
    firstname: "Artem",
    estimate: 4.5,
};

var st3 = {
    firstname: "Vlad",
    estimate: 4.0,
};

students.add(st);
students.add(st2);
students.add(st3);
students.remove("Ivan");
students.print();
var a = students.avgEstimate();
var stus = students.getStudentsByEstimate();
console.log(a);
console.log(stus);