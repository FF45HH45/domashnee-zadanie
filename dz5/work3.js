/*Задан массив объектов студентов вида
[ {name: “Ivan”, estimate: 4, course: 1, active: true},
{name: “Ivan”, estimate: 3, course: 1, active: true},
{name: “Ivan”, estimate: 2, course: 4, active: false},
{name: “Ivan”, estimate: 5, course: 2, active: true}  ]
- заполнить его более большим количеством студентов.
Написать функцию которая возвращает: среднюю оценку студентов в разрезе каждого курса:
{1: 3.2, 2: 3.5, 3: 4.5, 4: 3, 5: 4.5} с учетом только тех студентов которые активны.
 Посчитать количество неактивных студентов в разрезе каждого курса и общее количество неактивных студентов.
*/
var students= [
    {name: 'Ivan',
        estimate: 4,
        course: 1,
        active: true
    },
    {name: 'Maks ',
        estimate: 3,
        course: 1,
        active: true
    },
    {name: 'Anton',
        estimate: 2,
        course: 4,
        active: false
    },
    {name: 'Artem',
        estimate: 5,
        course: 2,
        active: true
    },
    {name: 'Vitalii',
        estimate: 5,
        course: 2,
        active: true
    },
    {name: 'Kate',
        estimate: 3,
        course: 3,
        active: true
    },
    {name: 'Vlad',
        estimate: 2,
        course: 4,
        active: true
    },
    {name: 'Mark',
        estimate: 5,
        course: 1,
        active: true
    },
    {name: 'Arsen',
        estimate: 4,
        course: 3,
        active: false
    },
    {name: 'Sasha',
        estimate: 4,
        course: 4,
        active: true
    },
    {name: 'Leon',
        estimate: 3,
        course: 2,
        active: true
    },
    {name: 'Mishel',
        estimate: 2,
        course: 1,
        active: false
    }
    ]
function calculateEstimatePerCourse(){
    let masValue = [];
    let courseValue = [];
    let estimateValue = [];
    let est;
    let resultValue = {};
    for(let i = 0; i < students.length; i++){
        if(students[i].active == true){
            masValue.push(students[i]);
        }
    }
    for(let i in masValue){
        courseValue.push(masValue[i].course)
    }
    let courseIndividual = [];
    for(let i in courseValue){
        if(courseIndividual.indexOf(courseValue[i]) == -1){
            courseIndividual.push(courseValue[i])
        }
    }
    for(let i in courseIndividual){
        resultValue[courseIndividual[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(resultValue).length; i++){
        let estimateMedium = [];
        for(let j in masValue){
            if(masValue[j].course == i){
                estimateMedium.push(masValue[j].estimate)
                resultValue[i] += masValue[j].estimate
            }
        }
        resultValue[i] = resultValue[i]/estimateMedium.length
    }
    console.log('Оценка средняя по курсу:')
    console.log(resultValue)
}
calculateEstimatePerCourse();


function inactiveStudents(){
    let inactiveStud = [];

    let courseStudents = {};
    for(let i in students){
        if(students[i].active == false){
            inactiveStud.push(students[i]);
        }
    }
    console.log('Общее количество неактивных студентов: ' + Object.keys(inactiveStud).length)

    let courseIndividual = [];
    for(let i in students){
        if(courseIndividual.indexOf(students[i].course) == -1){
            courseIndividual.push(students[i].course)
        }
    }
    for(let i in courseIndividual){
        courseStudents[courseIndividual[i]] = 0;
    }
    for(let i = 1; i <= Object.keys(courseStudents).length; i++){
        let inactiveStudPerCourse = [];
        for(let j in students){
            if(students[j].course == i && students[j].active == false){
                inactiveStudPerCourse.push(true)
            }
        }
        courseStudents[i] = inactiveStudPerCourse.length
    }
    console.log('Неактивных студентов по курсам:')
    console.log(courseStudents)
}
inactiveStudents();

