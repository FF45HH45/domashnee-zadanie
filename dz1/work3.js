var byte = 1;
var kb = 1024 * byte;
var mb = 1024 * kb;
var gb = 1024 * mb;

function execute(type, count) {
  if (type == 'байт') {
    var result = byte * count;
    console.log('Byte:' + result);
  }
  if (type == 'кб') {
    var result = kb * count;
    console.log('KByte:' + result);
  }
  if (type == 'мб') {
    var result = mb * count;
    console.log('MByte:' + result);
  }
  if (type == 'гб') {
    var result = gb * count;
    console.log('GByte:' + result);
  }
}

execute('байт', 5);
/*Переменная хранит в себе единицу измерения одно из возможных значений (Byte, KB, MB, GB),
Вторая переменная хранит в себе целое число.
В зависимости от того какая единица измерения написать скрипт, который выводит количество байт.
Для вычисления принимает счет что в каждой последующей единицы измерения хранится 1024 единиц более меньшего измерения.
*/
