/*Добавить еще одно свойство для студента - email адрес
Вывести mail адрес на ряду в списке студентов
Так же организовать возможность изменения по аналогии с именем
Написать регулярное выражение проверки введенных данных email
Написать валидацию проверку ввода данных - курс это любое целое число от 1 до 5, email - с помощью регулярки, имя может содержать только буквы не более 15 символов
Если при вводе в любую из форм введены невалидные данные - сообщать об этом в окне с ошибкой
Изменять данные физически тогда и только тогда, когда будут введены корректные
Добавить еще одно свойство: date - дата создания студента, брать текущее время клиента и записывать наряду с каждым студентом, выводить дату в списке со студентами - ее нельзя изменить.
    Добавить в статистику еще одно вычисление:  количество студентов которые были добавлены за последний час.
    Все данные хранить в localStorage касательно студентов
При открытии страницы выбирать из localstorage и показывать список студентов и статистику соответственно
Если localstorage не имеет никаких данных в себе, показывать что ничего нет при добавлении из формы нового студента - данные должны попасть и сохраниться в localstorage, при следующем открытии страницы - уже подтянуться сохраненные данные
*/
window.onload = function(){
    var students = [
        {name: 'Ivan',
            estimate: 4,
            course: 1,
            active: true,
            e_mail: 'ivan@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Maks ',
            estimate: 3,
            course: 1,
            active: true,
            e_mail: 'maks@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Anton',
            estimate: 2,
            course: 4,
            active: false,
            e_mail: 'anton@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Artem',
            estimate: 5,
            course: 2,
            active: true,
            e_mail: 'artem@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Vitalii',
            estimate: 5,
            course: 2,
            active: true,
            e_mail: 'vitalii@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Kate',
            estimate: 3,
            course: 3,
            active: true,
            e_mail: 'kate@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Vlad',
            estimate: 2,
            course: 4,
            active: true,
            e_mail: 'vlad@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Mark',
            estimate: 5,
            course: 1,
            active: true,
            e_mail: 'mark@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Arsen',
            estimate: 4,
            course: 3,
            active: false,
            e_mail: 'arsen@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Sasha',
            estimate: 4,
            course: 4,
            active: true,
            e_mail: 'sasha@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Leon',
            estimate: 3,
            course: 2,
            active: true,
            e_mail: 'leon@gmail.com',
            date: '08.11.2020 12:04'
        },
        {name: 'Mishel',
            estimate: 2,
            course: 1,
            active: false,
            e_mail: 'mishel@gmail.com',
            date: '08.11.2020 12:04'
        }
    ]

    function calculateEstimatePerCourse(){
        let masValue = [];
        let courseValue = [];
        let estimateValue = [];
        let est;
        let resultValue = {};
        for(let i = 0; i < students.length; i++){
            if(students[i].active == true){
                masValue.push(students[i]);
            }
        }
        for(let i in masValue){
            courseValue.push(masValue[i].course)
        }
        let courseIndividual = [];
        for(let i in courseValue){
            if(courseIndividual.indexOf(courseValue[i]) == -1){
                courseIndividual.push(courseValue[i])
            }
        }
        for(let i in courseIndividual){
            resultValue[courseIndividual[i]] = 0;
        }
        for(let i = 1; i <= Object.keys(resultValue).length; i++){
            let estimateMedium = [];
            for(let j in masValue){
                if(masValue[j].course == i){
                    estimateMedium.push(masValue[j].estimate)
                    resultValue[i] += masValue[j].estimate
                }
            }
            resultValue[i] = resultValue[i]/estimateMedium.length
        }
        console.log('Оценка средняя по курсу:')
        console.log(resultValue)
    }
    calculateEstimatePerCourse();


    function inactiveStudents(){
        let inactiveStud = [];

        let courseStudents = {};
        for(let i in students){
            if(students[i].active == false){
                inactiveStud.push(students[i]);
            }
        }
        console.log('Общее количество неактивных студентов: ' + Object.keys(inactiveStud).length)

        let courseIndividual = [];
        for(let i in students){
            if(courseIndividual.indexOf(students[i].course) == -1){
                courseIndividual.push(students[i].course)
            }
        }
        for(let i in courseIndividual){
            courseStudents[courseIndividual[i]] = 0;
        }
        for(let i = 1; i <= Object.keys(courseStudents).length; i++){
            let inactiveStudPerCourse = [];
            for(let j in students){
                if(students[j].course == i && students[j].active == false){
                    inactiveStudPerCourse.push(true)
                }
            }
            courseStudents[i] = inactiveStudPerCourse.length
        }
        console.log('Неактивных студентов по курсам:')
        console.log(courseStudents)
    }
    inactiveStudents();

    function getStud(){
        let list = document.querySelector('.list');
        let table = document.createElement('table');
        list.appendChild(table);
        let tr = document.createElement('tr');
        table.appendChild(tr);
        let th1 = document.createElement('th');
        let th2 = document.createElement('th');
        let th3 = document.createElement('th');
        let th4 = document.createElement('th');
        let th5 = document.createElement('th');
        let th6 = document.createElement('th');
        th1.innerHTML = 'First name';
        th2.innerHTML = 'Estimate';
        th3.innerHTML = 'Course';
        th4.innerHTML = 'Active';
        th5.innerHTML = 'E-mail';
        th6.innerHTML = 'date';
        tr.appendChild(th1);
        tr.appendChild(th2);
        tr.appendChild(th3);
        tr.appendChild(th5);
        tr.appendChild(th6);
        tr.appendChild(th4);

        for(let i in students){
            var trTr = document.createElement('tr')
            table.appendChild(trTr);
            var createStudents = document.createElement('td');
            createStudents.id = i +'n';
            var changeActive = document.createElement('input');
            var deleteStud = document.createElement('img');
            var estimateT = document.createElement('td');
            estimateT.id = i + 'e';
            var courseT = document.createElement('td');
            courseT.id = i + 'c';
            var e_mailT = document.createElement('td');
            var setDate = document.createElement('td');
            setDate.innerHTML = students[i].date;
            e_mailT.innerHTML = students[i].e_mail;
            estimateT.innerHTML = students[i].estimate;
            courseT.innerHTML = students[i].course;

            changeActive.type = 'submit';
            if(students[i].active == true){
                changeActive.value = 'active';
            }else if(students[i].active == false){
                changeActive.value = 'un active';
            }
            changeActive.id = 'changeActiveStudents';

            if(students[i].estimate <= 3){
                trTr.setAttribute('class', 'less');
            }else if(students[i].estimate >= 4 && students[i].estimate <5){
                trTr.setAttribute('class', 'midle');
            }else if(students[i].estimate >= 5){
                trTr.setAttribute('class', 'more');
            }
            createStudents.innerHTML = students[i].name;

            (function (i){
                createStudents.addEventListener('click', function(event){
                    let idName = i +'n';
                    let tdFirstName = document.getElementById(idName);
                    let inputName = document.createElement('input');
                    inputName.type = 'text';
                    inputName.className = 'input';
                    inputName.value = students[i].name;
                    tdFirstName.innerHTML = '';
                    tdFirstName.appendChild(inputName);
                    inputName.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            if(/[0-9]/.test(event.target.value) === false){
                                if(/[<>()\[\]\\.\-\_,;:\s@"]/.test(event.target.value) === false){
                                    if(event.target.value.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                                        students[i].name = event.target.value;
                                        list.innerHTML = '';
                                        createStud();
                                    }else{
                                        list.innerHTML = '';
                                        createStud();
                                        return alert('Имя может состоять только из букв!');
                                    }
                                }else{
                                    list.innerHTML = '';
                                    createStud();
                                    return alert('Имя может состоять только из букв!');
                                }
                            }else{
                                list.innerHTML = '';
                                createStud();
                                return alert('Имя может состоять только из букв!');
                            }
                        }
                    });
                    inputName.focus();
                });
            })(i);

            (function (i){
                estimateT.addEventListener('click', function(event){
                    let idEstimate = i +'e';
                    let tdEstimate = document.getElementById(idEstimate);
                    let inputEstimate = document.createElement('input');
                    inputEstimate.type = 'text';
                    inputEstimate.className = 'input';
                    inputEstimate.value = students[i].estimate;
                    tdEstimate.innerHTML = '';
                    tdEstimate.appendChild(inputEstimate);
                    inputEstimate.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            if(/[1-5]/.test(event.target.value) === true){
                                if(event.target.value.match(/[0-9]/g).length === 1){
                                    students[i].estimate = parseInt(event.target.value);
                                    list.innerHTML = '';
                                    createStud();
                                }else{
                                    list.innerHTML = '';
                                    createStud();
                                    return alert('Оценка может быть от 1 до 5!');
                                }
                            }else{
                                list.innerHTML = '';
                                createStud();
                                return alert('Оценка может быть от 1 до 5!');
                            }
                        }
                    });
                    inputEstimate.focus();
                });
            })(i);

            (function (i){
                courseT.addEventListener('click', function(event){
                    let idCourse = i +'c';
                    let tdCourse = document.getElementById(idCourse);
                    let inputCourse = document.createElement('input');
                    inputCourse.type = 'text';
                    inputCourse.className = 'input';
                    inputCourse.value = students[i].course;
                    tdCourse.innerHTML = '';
                    tdCourse.appendChild(inputCourse);
                    console.log('kurwa');
                    inputCourse.addEventListener('keydown', function(event){
                        if(event.keyCode === 13){
                            if(/[1-5]/.test(event.target.value) === true){
                                if(event.target.value.match(/[0-9]/g).length === 1){
                                    students[i].course = parseInt(event.target.value);
                                    list.innerHTML = '';
                                    createStud();
                                }else{
                                    list.innerHTML = '';
                                    createStud();
                                    return alert('Курс может быть от 1 до 5!');
                                }
                            }else{
                                list.innerHTML = '';
                                createStud();
                                return alert('Курс может быть от 1 до 5!');
                            }
                        }
                    });
                    inputCourse.focus();
                });
            })(i);

            (function (i, changeActive){
                changeActive.addEventListener('click', function(event){
                    if(changeActive.value == 'active'){
                        students[i].active = false;
                        list.innerHTML = '';
                        createStud();
                    }else if(changeActive.value == 'un active'){
                        students[i].active = true;
                        list.innerHTML = '';
                        getStud();
                    }
                })
            })(i, changeActive);
            (function (i){
                deleteStud.addEventListener('click', function(event){
                    students.splice(i, 1);
                    list.innerHTML = '';
                    getStud();
                });
            })(i);

            trTr.appendChild(createStudents);
            trTr.appendChild(estimateT);
            trTr.appendChild(courseT);
            trTr.appendChild(e_mailT);
            trTr.appendChild(setDate);
            trTr.appendChild(changeActive);
            trTr.appendChild(deleteStud);





        }

        if(document.querySelector('.less') == null && document.querySelector('.midle') == null && document.querySelector('.more') == null){
            let noStud = document.createElement('noStud');
            noStud.setAttribute('class', 'p');
            noStud.innerHTML = 'Студентов больше нет!';
            list.appendChild(noStud);
        }

        function avgEstimatePerCourse(){
            let courseArr = [];
            let courseUnique = [];
            let resultArr = {};

            for(let i in students){
                courseArr.push(students[i].course);
            }
            for(let i in courseArr){
                if(courseUnique.indexOf(courseArr[i]) == -1){
                    courseUnique.push(courseArr[i]);
                }
            }
            for(let i in courseUnique){
                resultArr[courseUnique[i]] = 0;
            }
            for(let i  = 1; i <= Object.keys(resultArr).length; i++){
                let estimateAverage = [];
                for(let j in students){
                    if(students[j].course == i){
                        estimateAverage.push(students[j].estimate);
                        resultArr[i] += students[j].estimate;
                    }
                }
                resultArr[i] = Math.round(resultArr[i]/estimateAverage.length);
            }
            console.log(resultArr);

            for(let i in resultArr){
                var averageOut = document.createElement('estimate');
                if(resultArr[i] <= 3){
                    averageOut.setAttribute('class', 'lessL');
                    averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                }else if(resultArr[i] >= 4 && resultArr[i] < 5){
                    averageOut.setAttribute('class', 'midleM');
                    averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                }else if(resultArr[i] >= 5){
                    averageOut.setAttribute('class', 'moreM');
                    averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultArr[i];
                }else if((resultArr[i] === NaN) === false){

                    averageOut.innerHTML = '';

                }

                list.appendChild(averageOut);
            }
        }avgEstimatePerCourse();

        function unActive(){
            let unActiveStud = [];
            var unActiveStudents = document.createElement('unActive');
            unActiveStudents.setAttribute('class', 'p');

            for(let i in students){
                if(students[i].active == false){
                    unActiveStud.push(students[i]);
                }
            }
            unActiveStudents.innerHTML = 'Всего неактивных студентов: ' + Object.keys(unActiveStud).length;
            list.appendChild(unActiveStudents);
        }unActive();

        function newStudentsPerLastHourse (newStudent){
            let data = new Date();
            let currentTime = data.getDate() + '.' + (data.getUTCMonth() + 1) + '.' + data.getFullYear() + ' ' + data.getHours();
            var newStudentsPerHourse = document.createElement('new_students');
            newStudentsPerHourse.setAttribute('class', 'p');
            for(let i in students){
                if(students[i].date.indexOf(currentTime) !== -1){
                    newStudent++;
                }
            }
            newStudentsPerHourse.innerHTML = 'Начиная с ' + data.getHours() + ':00 было добавлено: ' + newStudent + ' новых студентов';
            list.appendChild(newStudentsPerHourse);
        }newStudentsPerLastHourse(0);

    }getStud();

    function getNewStudents(){
        let submit = document.querySelector('.submit');

        submit.addEventListener('click', event => {
            let form = document.querySelector('form');
            let chbox = document.getElementById('chbox');
            let isActive;
            let liveDate = new Date();
            if(chbox.checked){
                isActive = true;
            }else{
                isActive = false;
            }
            for(let i = 0; i < 4; i++){
                console.log(i);

                if(/[0-9]/.test(form[i].value) === false){
                    if(/[<>()\[\]\\.\-\_,;:\s@"]/.test(form[i].value) === false){
                        if(form[i].value.match(/[A-Za-zА-Яа-я]/g).length <= 15){
                            if(/[1-5]/.test(form[i+1].value) === true){
                                if(form[i+1].value.match(/[0-9]/g).length === 1){
                                    if(/[1-5]/.test(form[i+2].value) === true){
                                        if(form[i+2].value.match(/[0-9]/g).length === 1){
                                            if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(form[i+3].value) === true){

                                            }else{
                                                alert('Вы ввели некоректный E-mail адрес!');
                                                break;
                                            }
                                        }else{
                                            alert('Курс может быть от 1 до 5!');
                                            break;
                                        }
                                    }else{
                                        alert('Курс может быть от 1 до 5!');
                                        break;
                                    }
                                }else{
                                    alert('Оценка может быть от 1 до 5!');
                                    break;
                                }
                            }else{
                                alert('Оценка может быть от 1 до 5!');
                                break;
                            }
                        }else{
                            alert('Имя должно иметь не более 15 букв!');
                            break;
                        }
                    }else{
                        alert('Имя может состоять только из букв!');
                        break;
                    }
                }else{
                    alert('Имя может состоять только из букв!');
                    break;
                }
                let newStudent = {name: form[i].value,
                    estimate: parseInt(form[i+1].value),
                    course: parseInt(form[i+2].value),
                    active: isActive,
                    e_mail: form[i+3].value,
                    date: liveDate.getDate() + '.' + (liveDate.getUTCMonth() + 1) + '.' + liveDate.getFullYear() + ' ' + liveDate.getHours() + ':' + liveDate.getMinutes()
                }
                students.push(newStudent);
                console.log(newStudent);
                console.log(students);
                console.log(isActive)
                break;
            }
            document.querySelector('.list').innerHTML = '';
            createStud();
        })
    }getNewStudents();


}
