const items = [
    [1, 2],
    [3, 4],
    [5, 6]
];

const result = {
    targetIndex: 0,
    sum: 0
};

for(let i = 0; i < items.length; i++) {
    const currentSum = items[i].reduce((prev, current) => prev + current);
    if (result.sum <= currentSum) {
        result.sum = currentSum;
        result.targetIndex = i + 1;
    }
}

console.log(`Номер столбца двумерного массива сумма которого является максимальной - ${result.targetIndex}`);
console.log(`Сумма - ${result.sum}`);




//Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки)