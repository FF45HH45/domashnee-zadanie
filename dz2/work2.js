function factorial(n) {
  if (n == 0) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}
let m = factorial(6);
console.log(m);

//Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
