var str = 'Привет мир !';
function reverseString(str) {
  return str.split('').reverse().join('');

}

console.log('str:', str);
console.log('reverse:', reverseString(str));

//Переменная содержит в себе строку. Вывести строку в обратном порядке.
