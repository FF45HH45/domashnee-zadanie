/*
В задании из пятого урока, взять массив со студентами и вывести их на страницу согласно сверстанной HTML-структуре, рядом с каждым студентом
вывести крестик - по нажатию на который студент будет удален (удаляется как со страницы, так и с объекта), если был удален последний студент
написать текстовое сообщение (“Студенты не найдены”)
Вывести статистику средних оценок в разрезе курса и статистику по количеству неактивных студентов в разрезе каждого курса и общее количество неактивных студентов
Добавить текстовое поле ввода - ввод имени студента, поле ввода для курса, оценки и checkbox для активности студента, по нажатии на кнопку “Добавить” - студент новый добавляется в список студентов

*/
window.onload = function(){
    var students = [
        {name: 'Ivan',
            estimate: 4,
            course: 1,
            active: true
        },
        {name: 'Maks ',
            estimate: 3,
            course: 1,
            active: true
        },
        {name: 'Anton',
            estimate: 2,
            course: 4,
            active: false
        },
        {name: 'Artem',
            estimate: 5,
            course: 2,
            active: true
        },
        {name: 'Vitalii',
            estimate: 5,
            course: 2,
            active: true
        },
        {name: 'Kate',
            estimate: 3,
            course: 3,
            active: true
        },
        {name: 'Vlad',
            estimate: 2,
            course: 4,
            active: true
        },
        {name: 'Mark',
            estimate: 5,
            course: 1,
            active: true
        },
        {name: 'Arsen',
            estimate: 4,
            course: 3,
            active: false
        },
        {name: 'Sasha',
            estimate: 4,
            course: 4,
            active: true
        },
        {name: 'Leon',
            estimate: 3,
            course: 2,
            active: true
        },
        {name: 'Mishel',
            estimate: 2,
            course: 1,
            active: false
        }
    ]

    function getNewStudents(){
        let list = document.querySelector('.list');
        let pl = document.createElement('pl');
        list.appendChild(pl);
        let form = document.createElement('form');
        form.innerHTML = 'Введите имя студента';
        pl.appendChild(form);
        let studName = document.createElement('input');

        form.appendChild(studName);
        let input = document.createElement('input');
        input.setAttribute('type', 'submit');
        input.setAttribute('value', 'Submit');
        form.appendChild(input)
    }getNewStudents();

    function calculateEstimatePerCourse(){
        let masValue = [];
        let courseValue = [];
        let estimateValue = [];
        let est;
        let resultValue = {};
        for(let i = 0; i < students.length; i++){
            if(students[i].active == true){
                masValue.push(students[i]);
            }
        }
        for(let i in masValue){
            courseValue.push(masValue[i].course)
        }
        let courseIndividual = [];
        for(let i in courseValue){
            if(courseIndividual.indexOf(courseValue[i]) == -1){
                courseIndividual.push(courseValue[i])
            }
        }
        for(let i in courseIndividual){
            resultValue[courseIndividual[i]] = 0;
        }
        for(let i = 1; i <= Object.keys(resultValue).length; i++){
            let estimateMedium = [];
            for(let j in masValue){
                if(masValue[j].course == i){
                    estimateMedium.push(masValue[j].estimate)
                    resultValue[i] += masValue[j].estimate
                }
            }
            resultValue[i] = resultValue[i]/estimateMedium.length
        }
        console.log('Оценка средняя по курсу:')
        console.log(resultValue)
    }
    calculateEstimatePerCourse();


    function inactiveStudents(){
        let inactiveStud = [];

        let courseStudents = {};
        for(let i in students){
            if(students[i].active == false){
                inactiveStud.push(students[i]);
            }
        }
        console.log('Общее количество неактивных студентов: ' + Object.keys(inactiveStud).length)

        let courseIndividual = [];
        for(let i in students){
            if(courseIndividual.indexOf(students[i].course) == -1){
                courseIndividual.push(students[i].course)
            }
        }
        for(let i in courseIndividual){
            courseStudents[courseIndividual[i]] = 0;
        }
        for(let i = 1; i <= Object.keys(courseStudents).length; i++){
            let inactiveStudPerCourse = [];
            for(let j in students){
                if(students[j].course == i && students[j].active == false){
                    inactiveStudPerCourse.push(true)
                }
            }
            courseStudents[i] = inactiveStudPerCourse.length
        }
        console.log('Неактивных студентов по курсам:')
        console.log(courseStudents)
    }
    inactiveStudents();

    function getStud(){
        let list = document.querySelector('.list');
        for(let i in students){

            var getStudents = document.createElement('stud');
            getStudents.setAttribute('class', 'p');
            getStudents.innerHTML = students[i].name + ' x';
            (function (i){
                getStudents.addEventListener('click', function(event){
                    students.splice(i, 1);
                    list.innerHTML = '';
                    getStud();
                });
            })(i);
            list.appendChild(getStudents);

        }
        if(document.querySelector('stud') == null){
            let noStud = document.createElement('noStud');
            noStud.setAttribute('class', 'p');
            noStud.innerHTML = 'Студентов больше нет!';
            list.appendChild(noStud);
        }
        function mediumEstimateCourse(){
            let courseValue = [];
            let courseIndividual = [];
            let resultValue = {};

            for(let i in students){
                courseValue.push(students[i].course);
            }
            for(let i in courseValue){
                if(courseIndividual.indexOf(courseValue[i]) == -1){
                    courseIndividual.push(courseValue[i]);
                }
            }
            for(let i in courseIndividual){
                resultValue[courseIndividual[i]] = 0;
            }
            for(let i  = 1; i <= Object.keys(resultValue).length; i++){
                let estimateMedium = [];
                for(let j in students){
                    if(students[j].course == i){
                        estimateMedium.push(students[j].estimate);
                        resultValue[i] += students[j].estimate;
                    }
                }
                resultValue[i] = Math.round(resultValue[i]/estimateMedium.length);
            }
            console.log(resultValue);

            for(let i in resultValue){
                var averageOut = document.createElement('estimate');
                averageOut.setAttribute('class', 'p');
                averageOut.innerHTML = 'Средняя оценка ' + i + '-ого курса: ' + resultValue[i];
                list.appendChild(averageOut);
            }
        }mediumEstimateCourse;

        function unActive(){
            let unActiveStud = [];
            var unActiveStudents = document.createElement('unActive');
            unActiveStudents.setAttribute('class', 'p');

            for(let i in students){
                if(students[i].active == false){
                    unActiveStud.push(students[i]);
                }
            }
            unActiveStudents.innerHTML = 'Всего неактивных студентов: ' + Object.keys(unActiveStud).length;
            list.appendChild(unActiveStudents);
        }unActive();
    }getStud();

}
