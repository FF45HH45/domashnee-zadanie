//dz4(2 занятие)
/*
Использовать функцию из предыдущего задания чтобы получить массив из нужного количества значений.
Найти процентное соотношение отрицательных, положительных и нулевых элементов массива.
*/
var plus = [];
var minus = [];
var zero = [];
var mas = [];
var N = -2;
var M = 11;
var k = 7;


function res(a){
    if(a < k){
        let i;
        i = Math.floor((Math.random()*Math.random()*(M - N +1) + N));
        mas.push(i);

    }else if(a > k){
        console.log(mas);
        return;
    }
    res(++a);
}
res(0);
for(let item in mas)
{
    if(item < 0){
        minus.push(item);
    }else if(item > 0){
        plus.push(item);
    }else if(item == 0){
        zero.push(item);
    }
}

var procent = k/100
console.log('Процент положительных чисел ' + (plus.length/procent) + '%');
console.log('Процент отрицательных чисел ' + (minus.length/procent) + '%');
console.log('Процент нолевых элементов ' + (zero.length/procent) + '%');
//Dz(3(5)
//Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки)
const items = [
    [1, 2],
    [3, 4],
    [5, 6]
];

const result = {
    targetIndex: 0,
    sum: 0
};
let i = 0;
for (let item of items){
    const currentSum = item.reduce((prev, current) => prev + current);
    if (result.sum <= currentSum) {
        result.sum = currentSum;
        result.targetIndex = i + 1;
    }
    i++;
}

console.log(`Номер столбца двумерного массива сумма которого является максимальной - ${result.targetIndex}`);
console.log(`Сумма - ${result.sum}`);





/*
Все предыдущий задания на циклы - написать с помощью циклов for in и/или for of.
 */